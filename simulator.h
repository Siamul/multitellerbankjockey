#include <queue>
#include <iostream>
#include <string>
#include <cstdlib>
#include <math.h>
#include <list>
using namespace std;

typedef double t_simtime;

class Simulator;
struct Customer;

enum EventType {
    EXIT,
    ARRIVAL,
    DEPARTURE
    // Define event types of your own
};


/*function for exponential distribution (added) */
double expo(double beta, int* stream)
{
	double U = (double) (rand() % (RAND_MAX - 1) + 1) / RAND_MAX; //the range is (0,1) 0 and 1 exclusive
	/*use rand_r or rand_s, if available, using stream provided, here rand() is used for portability.*/
	//double U = (double) (rand_r() % (RAND_MAX - 1) + 1) / RAND_MAX;
	//double U = (double) (rand_s() % (RAND_MAX - 1) + 1) / RAND_MAX;
	/*We can also use better random number generators here from C++11*/
	//cout << U << endl;
	return -beta*log(U);
}

/* Pure Virtual class for all events */


class Event {
    private:
        t_simtime eventTime;
        EventType eventType;
        string eventName;

    public:
        Event(t_simtime time, EventType type, string name) {
            this->eventTime = time;
            this->eventType = type;
            this->eventName = name;
        }

        Event(t_simtime time) {
            this->eventTime = time;
        }

        t_simtime getEventTime() { return this->eventTime; }
        EventType getEventType() { return this->eventType; }
        string getEventName ()   { return this->eventName; }

        friend ostream & operator << (ostream & out, Event &e);

        /* All event subclass must override this function */
        virtual void processEvent(Simulator *sim) = 0;
};

ostream & operator << (ostream & out, Event & e) {
    out << e.getEventName() << " " << e.getEventTime() ;
    return out;
}


/* Comparintg two events: event with lower time goes first */
class CompareEvent {
    public:
        bool operator() (Event* &e1, Event* &e2) {
            //cout << *e1 << " " << *e2 << endl;
            return e1->getEventTime() > e2->getEventTime();
        }
};

class Simulator {
    private:
        priority_queue<Event*, vector<Event*>, CompareEvent>  *eventQueue;
        deque<Customer* > **tellerQueue;
        double* delay;
        t_simtime simclock;
        int tellerNumber;

    public:
    	int* tellerBusy;
    	int peopleInQueue;
    	double totalQueueLength;
    	double previousEventTime;
        void scheduleEvent(Event *event);
        double getAvgQueue();
        void setSimulationEndTime(t_simtime endTime);
        void updateDelay(int customer, double delay); //added
        void pushCustomer(Customer* customer, int teller); //added
        Customer* popCustomer(int teller); //added
        int queueSize(int teller); //added
        int getTellerNumber();
        double* getDelay();
        void jockey();
        void run();
        t_simtime now();

		Simulator();
        Simulator(int tellerNumber);
        ~Simulator();
};

struct Customer {
	int customerNumber;
	double arrivalTime;
};

class EndEvent : public Event {
    public:
        EndEvent(t_simtime time, EventType type, string name);
        EndEvent(t_simtime time);
        void processEvent(Simulator *sim);
};

class DepartureEvent : public Event {
	public:
		DepartureEvent(t_simtime time, EventType type, string name);
		DepartureEvent(t_simtime time);
		void processEvent(Simulator *sim);
};


class ArrivalEvent : public Event {
    public:
        ArrivalEvent(t_simtime time, EventType type, string name);
        ArrivalEvent(t_simtime time);
        void processEvent(Simulator *sim);
};

/* You need to add more events as they are required */
