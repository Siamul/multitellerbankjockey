#include "simulator.h"
#include <math.h>
#include <cstdlib>
#include <iostream>
#include <string>
#include <queue>
#include <ctime>
#include <fstream>
using namespace std;

//    Simulation and Modelling Assignment - Multiteller Bank with Jockeying
//    Problem Description:
//    ->Bank accepts new customers from 9 am to 5 pm so the total time in minutes is 480.
//    ->Customer Inter-arrival times -> IID with expo(1) -> beta is given as 1 minute
//    ->Customer Service times -> IID with expo(4.5) -> beta is given as 4.5 minutes
//    ->Each teller has a separate queue.
//    ->Arriving customer enters the leftmost shortest queue.
//    ->No customer is present when the bank opens.
//    ->The simulation is carried out for different number of tellers -> 4,5,6,7
//    ->For each value of number of tellers, the simulation is repeated 100 times.
//    ->Jockeying occurs (when nj > ni + 1, customer moves from nj to ni) from the closest leftmost queue.

//****I HAVE KEPT THE CUSTOMER NUMBER IN THE EVENT NAMES.FOR EXAMPLE: A34 MEANS ARRIVAL OF 34th CUSTOMER, D76 MEANS DEPARTURE OF 76TH CUSTOMER
//****OUTPUT IS SHOWN IN CONSOLE AS WELL AS IN OUTPUT.TXT

#define arrivalExpo 1
#define departureExpo 4.5
#define totalTime 480
#define delayQueueSize (int)((totalTime/arrivalExpo)*1.5)

int arrivalNumber = 1;
int* arrivalStream;
int* departureStream;
int actualDelayQueueSize = 0;

Simulator::Simulator() {
    eventQueue =  new priority_queue<Event*, vector<Event*>, CompareEvent>();
    tellerQueue = new deque<Customer* >*[tellerNumber];
    tellerBusy = new int[tellerNumber];
    tellerNumber = 4;
    arrivalNumber = 0;
    actualDelayQueueSize = 0;
    previousEventTime  = 0;
    totalQueueLength = 0;
    simclock = 0;
    peopleInQueue = 0;
    delay = new double[delayQueueSize];
    for(int i = 0; i<delayQueueSize; i++) delay[i] = 0;
    for(int i = 0; i < tellerNumber; i++)
    {
    	tellerBusy[i] = -1;
    	tellerQueue[i] = new deque<Customer* >();
	}
}

Simulator::Simulator(int tellerNumber) {
	this->tellerNumber = tellerNumber;
    eventQueue =  new priority_queue<Event*, vector<Event*>, CompareEvent>();
    tellerQueue = new deque<Customer* >*[tellerNumber];
    tellerBusy = new int[tellerNumber];
    arrivalNumber = 0;
    actualDelayQueueSize = 0;
    previousEventTime  = 0;
    totalQueueLength = 0;
    simclock = 0;
    peopleInQueue = 0;
    delay = new double[delayQueueSize];
    for(int i = 0; i<delayQueueSize; i++) delay[i] = 0;
    for(int i = 0; i < tellerNumber; i++)
    {
    	tellerBusy[i] = -1;
    	tellerQueue[i] = new deque<Customer* >();
	}
}

int Simulator::getTellerNumber()
{
	return tellerNumber;
}

t_simtime Simulator::now() {
    return this->simclock;
}


void Simulator::scheduleEvent(Event *event) {
    this->eventQueue->push(event);
}

double* Simulator::getDelay()
{
	return this->delay;
}

double average(double* value, int size)
{
	double sum = 0;
	for(int i = 0; i<size; i++)
	{
		sum += value[i];
	}
//	cout << "average delay is " << (double)(sum/size) << endl;
	return (double)(sum/size);
}

double ssqn(double* value, double mean, int size)
{
	double squareSum = 0;
	for(int i = 0; i<size; i++)
	{
		squareSum += (value[i] - mean)*(value[i] - mean);
	}
	squareSum = (double) (squareSum / (size - 1));
	return squareSum;
}

int Simulator::queueSize(int teller)
{
	if(this->tellerQueue[teller] != NULL)
		return this->tellerQueue[teller]->size();
	else
		return 0;
}

void Simulator::updateDelay(int customer, double delay)
{
	this->delay[customer] = delay;
}

void Simulator::pushCustomer(Customer* customer, int teller)
{
	this->tellerQueue[teller]->push_back(customer);
	this->peopleInQueue++;
//	cout << peopleInQueue << endl;
}

Customer* Simulator::popCustomer(int teller)
{
	Customer* customer = this->tellerQueue[teller]->front();
	this->tellerQueue[teller]->pop_front();
	this->peopleInQueue--;
//	cout << peopleInQueue << endl;
	return customer;
}

void Simulator::jockey()
{
	int k;
	for(int i = tellerNumber - 1; i>=0; i--)
	{
		k = i-1;
		if(k<0) k = tellerNumber + k;
		for(int j = 0; j < tellerNumber - 1; j++)
		{
			if(queueSize(k) - queueSize(i) >= 2)
			{
				tellerQueue[i]->push_back(tellerQueue[k]->back());
				tellerQueue[k]->pop_back();
			}
			k--;
			if(k<0) k = tellerNumber + k;
		}
	}

}

void Simulator::run() {
    //cout << "Simulator started.\n" << endl;

    while (eventQueue->empty() == false) {
        Event *event = this->eventQueue->top();
        this->eventQueue->pop();

       // cout << (*event) << endl;

        if (event == NULL || event->getEventType() == EXIT)
            break;

        this->simclock = event->getEventTime();
        event->processEvent(this);
        delete event;
    }
}

double Simulator::getAvgQueue()
{
//	cout << totalQueueLength << endl;
	return (double)(totalQueueLength / 480);
}

void Simulator::setSimulationEndTime(t_simtime endTime) {
    Event *endEvent = new EndEvent(endTime);
    this->scheduleEvent(endEvent);
}

Simulator::~Simulator() {
    if (eventQueue) {
        while (!eventQueue->empty()) {
           Event *event = eventQueue->top();
           eventQueue->pop();
           delete event;
       }
       delete eventQueue;
    }
//	cout << "event queue deleted"  << endl;
    for(int i = 0; i < tellerNumber; i++)
	{
	 	delete tellerQueue[i];
	}
	delete tellerQueue;
//	cout << "tellerQueue deleted" << endl;
	delete delay;
//	cout << "delay list deleted" << endl;
	delete tellerBusy;
}

// Event classes

EndEvent::EndEvent(t_simtime time, EventType type, string name): Event (time, type, name) { }
EndEvent::EndEvent(t_simtime time): Event (time, EXIT, string("EXIT")) { }
void EndEvent::processEvent(Simulator *sim) {


}

DepartureEvent::DepartureEvent(t_simtime time, EventType type, string name): Event (time, type, name) { }
DepartureEvent::DepartureEvent(t_simtime time): Event (time, DEPARTURE, string("DEPARTURE")) { }
void DepartureEvent::processEvent(Simulator *sim) {
	double interEventTime = sim->now() - sim->previousEventTime;
	//cout << "Event: "<< interEventTime << endl;
	if(interEventTime < 0) cout << "negative in departure" << endl;
	sim->previousEventTime = sim->now();
	sim->totalQueueLength += (double)(interEventTime*sim->peopleInQueue);
	int customer = atoi(this->getEventName().substr(1).c_str());
	//cout << "customer problem" << endl;
	int i;
	for(i = 0; i < sim->getTellerNumber(); i++)
	{
	//	cout << "I am in loop" << endl;
		if(sim->tellerBusy[i] == customer)
		{
			break;
		}
	}
	if(sim->queueSize(i) == 0)
	{
		sim->tellerBusy[i] = -1;
	}
	else
	{
	//	cout << "here I am" << endl;
		Customer* customer = sim->popCustomer(i);
		sim->tellerBusy[i] = customer->customerNumber;
		double delay = sim->now() - customer->arrivalTime;
	//	cout << "delay is: " << delay << endl;
		sim->updateDelay(customer->customerNumber, delay);
	//	cout << "delay updated" << endl;
		actualDelayQueueSize = customer->customerNumber;
	//	cout << "about to declare char" << endl;
		char* val = new char[100];
	//	cout << "declared char" << endl;
		itoa(customer->customerNumber, val, 10);
	//	cout << "itoa done" << endl;
		string name(val);
	//	cout << "string declared" << endl;
		string departureName = "D" + name;
		double departureTime = expo(departureExpo, departureStream);
	//	cout << "about to schedule" << endl;
		sim->scheduleEvent(new DepartureEvent(sim->now()+departureTime, DEPARTURE, departureName));
	//	cout << "scheduled" << endl;
	}
	//cout << "about to jockey" << endl;
	sim->jockey();
	//cout << "jockey complete" << endl;
}


ArrivalEvent::ArrivalEvent(t_simtime time, EventType type, string name): Event (time, type, name) { }
ArrivalEvent::ArrivalEvent(t_simtime time): Event (time, ARRIVAL, string("ARRIVAL")) { }

/* A dummmy process of arrival events */
void ArrivalEvent::processEvent(Simulator *sim) {
	//default_random_engine gen; //requires -std=c++11 as compiler option
	//exponentialDistribution<double> expo(1);
	//double arrivalTime = expo(gen);
	double interEventTime = (double)(sim->now() - sim->previousEventTime);
	//cout << "Event: "<< interEventTime << endl;
	if(interEventTime < 0) cout << "negative in arrival" << endl;
	sim->previousEventTime = sim->now();
	sim->totalQueueLength += (double)(interEventTime*sim->peopleInQueue);
	double arrivalTime = expo(arrivalExpo, arrivalStream); //function added in simulator.h
	char* val = new char[100];
	itoa(arrivalNumber, val, 10);
	string name(val);
	string arrivalName = "A" + name;
    sim->scheduleEvent(new ArrivalEvent(sim->now() + arrivalTime, ARRIVAL, arrivalName));
    int minQueueSize = sim->queueSize(0);
    int minQueue = 0;
	for(int i = 0; i< sim->getTellerNumber(); i++)
	{
		if(sim->tellerBusy[i] == -1)
		{
			sim->updateDelay(arrivalNumber, 0);
			actualDelayQueueSize = arrivalNumber;
			sim->tellerBusy[i] = arrivalNumber++;
			double departureTime = expo(departureExpo, departureStream);
	        string departureName = "D"+ name;
			sim->scheduleEvent(new DepartureEvent(sim->now()+departureTime, DEPARTURE, departureName));
			return;
		}
		else if(sim->queueSize(i) < minQueueSize)
		{
			minQueueSize = sim->queueSize(i);
			minQueue = i;
		}
	}
	Customer* newCustomer = new Customer();
	newCustomer->customerNumber = arrivalNumber++;
	newCustomer->arrivalTime = sim->now();
	sim->pushCustomer(newCustomer, minQueue);
}


int main(int argc, char **argv) {
	srand(time(NULL));
	int rand1 = rand();
	int rand2 = rand();
	arrivalStream = &rand1;
	departureStream = &rand2;
	double averageQueue[7];
	double averageDelay[7];
	double averageSSQN[7];
	double averageNumber[7];
	for(int i = 0; i<7; i++)
	{
		averageQueue[i] = 0;
		averageDelay[i] = 0;
		averageSSQN[i] = 0;
		averageNumber[i] = 0;
	}
	for(int i = 4; i <= 7; i++)
	{
		//cout << "before declaring new sim" << endl;
		//cout << "new sim for " << i << endl;
		//sim->setTellerNumber(i);
		//cout << sim->getTellerNumber() << endl;
		for(int j = 0; j<99; j++)
		{
		//	cout << "trying to declare new sim" << endl;
			Simulator* sim = new Simulator(i);
        	sim->scheduleEvent(new ArrivalEvent(0));
			sim->setSimulationEndTime(480);
       		sim->run();
       		//cout << "trying to get Delay" << endl;
       		double* delay = sim->getDelay();
       		double mean = average(delay, actualDelayQueueSize);
       		averageDelay[i] += mean;
       		averageSSQN[i] += ssqn(delay, mean, actualDelayQueueSize);
       		averageQueue[i] += sim->getAvgQueue();
       		averageNumber[i] += actualDelayQueueSize;
       	//	cout << "trying to delete sim" << endl;
       		delete sim;
		}
		averageDelay[i] /= 100.0;
		averageQueue[i] /= 100.0;
		averageSSQN[i] /= 100.0;
		averageNumber[i] /= 100.0;


		//cout << "sim deleted" << endl;
	}
	for(int i = 4; i <= 7; i++)
	{
		cout << "=========================================================" << endl;
		cout << "Teller " << i << endl;
		cout << "=========================================================" << endl;
		cout << "average delay: " << averageDelay[i] <<endl;
		cout << "---------------------------------------------------------" << endl;
		cout << "average queue length: " << averageQueue[i] << endl;
		cout << "---------------------------------------------------------" << endl;
		cout << "90% confidence interval: " << averageDelay[i] - 1.645*sqrt(averageSSQN[i]/averageNumber[i]) << " < delay < " << averageDelay[i] + 1.645*sqrt(averageSSQN[i]/averageNumber[i])  << endl;
	}
	ofstream output;
	output.open("output.txt");
	for(int i = 4; i <= 7; i++)
	{
		output << "=========================================================" << endl;
		output << "Teller " << i << endl;
		output << "=========================================================" << endl;
		output << "average delay: " << averageDelay[i] <<endl;
		output << "---------------------------------------------------------" << endl;
		output << "average queue length: " << averageQueue[i] << endl;
		output << "---------------------------------------------------------" << endl;
		output << "90% confidence interval: " << averageDelay[i] - 1.645*sqrt(averageSSQN[i]/averageNumber[i])  << " < delay < " << averageDelay[i] + 1.645*sqrt(averageSSQN[i]/averageNumber[i])  << endl;
	}
	output.close();
    return 0;
}

